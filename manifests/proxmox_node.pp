class roles::proxmox_node {
  include ::profiles::proxmox_node

  include ::lysntp
  include ::lyslogclient
  include ::lysnetwork::ssh
  include ::profiles::monitoring_agents
  include ::profiles::puppet_agent
}
