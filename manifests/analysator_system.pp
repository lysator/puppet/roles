class roles::analysator_system {
  class { '::profiles::service':
    use_legacy_lyslogin => false,
  }
  include ::analysator::munge
  include ::analysator::system
  include ::analysator::prometheus
  include ::analysator::packages::basic
  include ::lysipmi
}
