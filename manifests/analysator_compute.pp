class roles::analysator_compute {
  include ::lysntp
  include ::lyslogclient
  include ::packages::base
  include ::lysnetwork::ssh
  include ::prometheus::node_exporter
  include ::profiles::zabbix_agent
  include ::profiles::analysator_compute
  include ::profiles::puppet_agent
}
