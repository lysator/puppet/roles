class roles::freeipa {
  class { '::lysnetwork::public_ip':
    manage_resolvconf => false,
  }
  require ::epel
  require ::lysnetwork::iptables_default_deny
  include ::lysnetwork::fail2ban
  include ::lyslogclient
  include ::packages::base
  include ::profiles::monitoring_agents
  include ::profiles::puppet_agent
  include ::profiles::auto_update
  include ::profiles::freeipa
}
