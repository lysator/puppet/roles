class roles::puppetmaster {
  include ::profiles::puppetmaster
  include ::profiles::puppetboard
  include ::profiles::puppet_agent
  include ::profiles::monitoring_agents
  include ::lysnetwork::iptables_strict
  firewall { '100 accept http(s) from lysator':
    proto  => 'tcp',
    jump   => 'accept',
    dport  => ['80','443'],
    source => '130.236.254.0/24',
  }

}
