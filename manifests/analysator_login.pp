class roles::analysator_login {
  include ::lysntp
  include ::lyslogclient
  include ::packages::base
  include ::lysnetwork::ssh
  include ::lysipmi
  include ::profiles::analysator_login
}
